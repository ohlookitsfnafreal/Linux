%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Arch Linux

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% TODOs
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Mandatory Access Control

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Initial
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Wireless
%%%%%%%%%%%%%%%%%%%%

iw dev

wifi-menu -o 'wlp3s0'

%%%%%%%%%%%%%%%%%%%%
%%%%% Check Internet
%%%%%%%%%%%%%%%%%%%%

ping archlinux.org

%%%%%%%%%%%%%%%%%%%%
%%%%% Install over SSH
%%%%%%%%%%%%%%%%%%%%

ifconfig

passwd
systemctl start 'sshd'

ssh 'root@192.168.1.116'

%%%%%%%%%%%%%%%%%%%%
%%%%% Set Time
%%%%%%%%%%%%%%%%%%%%

timedatectl set-ntp 'true'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% GPT/EFI Systems
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Nuke
%%%%%%%%%%%%%%%%%%%%

lsblk

sgdisk --zap-all '/dev/sda'
dd if='/dev/zero' of='/dev/sda'
reboot

%%%%%%%%%%%%%%%%%%%%
%%%%% Create Partitions
%%%%%%%%%%%%%%%%%%%%

gdisk '/dev/sda'

- n (create new partition)
- 1 (partition number)
- Enter (use default first sector)
- +512MiB (last sector)
- ef00 (partition type)

- n (create new partition)
- 2 (partition number)
- Enter (use default first sector)
- Enter (use default last sector)
- 8e00 (partition type)

- p (verify two partitions):
1 512.0 MiB EF00 EFI System
2 (remaining space) 8E00 Linux LVM

- w (write changes to disk; confirm with y)

%%%%%%%%%%%%%%%%%%%%
%%%%% Encryption (Hammerstorm, Spinecrack, Hailrake)
%%%%%%%%%%%%%%%%%%%%

cryptsetup -v -c 'aes-xts-plain64' -s '512' -h 'sha512' -i '5000' --use-random luksFormat '/dev/sda2'
cryptsetup luksOpen '/dev/sda2' lvm

%%%%%%%%%%%%%%%%%%%%
%%%%% Setup LVM
%%%%%%%%%%%%%%%%%%%%

- Use '/dev/mapper/sda2' for pvcreate and vgcreate on unencrypted setups

pvcreate '/dev/mapper/lvm'
vgcreate 'arch' '/dev/mapper/lvm'
lvcreate -L 8GB 'arch' -n 'swap'
lvcreate -l 100%FREE 'arch' -n 'root'

%%%%%%%%%%%%%%%%%%%%
%%%%% Format Partitions
%%%%%%%%%%%%%%%%%%%%

mkfs.fat -F32 '/dev/sda1'
mkfs.xfs '/dev/mapper/arch-root'

%%%%%%%%%%%%%%%%%%%%
%%%%% Mount Partitions
%%%%%%%%%%%%%%%%%%%%

mkswap '/dev/mapper/arch-swap'
swapon '/dev/mapper/arch-swap'

mount '/dev/mapper/arch-root' '/mnt'
mkdir -p '/mnt/boot'
mount '/dev/sda1' '/mnt/boot'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% MBR/Legacy Unencrypted Systems
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Nuke
%%%%%%%%%%%%%%%%%%%%

lsblk

sgdisk --zap-all '/dev/sda'
dd if='/dev/zero' of='/dev/sda'
reboot

%%%%%%%%%%%%%%%%%%%%
%%%%% Create Partitions
%%%%%%%%%%%%%%%%%%%%

fdisk '/dev/sda'

- o (create empty DOS table)

- n (create new partition)
- p (primary partition type)
- 1 (partition number)
- Enter (use default first sector)
- +512MiB (last sector)

- n (create new partition)
- p (primary partition type)
- 2 (partition number)
- Enter (use default first sector)
- Enter (use default last sector)

- t (change partition type)
- 2 (partition number)
- 8e (Linux LVM)

- p (verify two partitions):
1 512M 83 Linux
2 (remaining space) 8e Linux LVM

- w (write changes to disk)

%%%%%%%%%%%%%%%%%%%%
%%%%% Setup LVM
%%%%%%%%%%%%%%%%%%%%

pvcreate '/dev/sda2'
vgcreate 'arch' '/dev/sda2'
lvcreate -L 8GB 'arch' -n 'swap'
lvcreate -l 100%FREE 'arch' -n 'root'

%%%%%%%%%%%%%%%%%%%%
%%%%% Format Partitions
%%%%%%%%%%%%%%%%%%%%

mkfs.ext2 '/dev/sda1'
mkfs.xfs '/dev/mapper/arch-root'

%%%%%%%%%%%%%%%%%%%%
%%%%% Mount Partitions
%%%%%%%%%%%%%%%%%%%%

mkswap '/dev/mapper/arch-swap'
swapon '/dev/mapper/arch-swap'

mount '/dev/mapper/arch-root' '/mnt'
mkdir -p '/mnt/boot'
mount '/dev/sda1' '/mnt/boot'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Install
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Base Install
%%%%%%%%%%%%%%%%%%%%

pacstrap -i '/mnt' 'base' 'base-devel' --noconfirm

%%%%%%%%%%%%%%%%%%%%
%%%%% Generate fstab
%%%%%%%%%%%%%%%%%%%%

genfstab -L '/mnt' >> '/mnt/etc/fstab'

%%%%%%%%%%%%%%%%%%%%
%%%%% chroot
%%%%%%%%%%%%%%%%%%%%

arch-chroot '/mnt' '/bin/bash'

%%%%%%%%%%%%%%%%%%%%
%%%%% Set Locale
%%%%%%%%%%%%%%%%%%%%

nano '/etc/locale.gen'

- Uncomment
-------------------------
en_US.UTF-8 UTF-8
-------------------------

locale-gen

nano '/etc/locale.conf'

-------------------------
LANG=en_US.UTF-8
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Set Time
%%%%%%%%%%%%%%%%%%%%

ln -s '/usr/share/zoneinfo/America/New_York' '/etc/localtime'
hwclock --systohc --utc

%%%%%%%%%%%%%%%%%%%%
%%%%% Pacman
%%%%%%%%%%%%%%%%%%%%

nano '/etc/pacman.conf'

- Uncomment
- Multilib doesn't exist on i686 installs
-------------------------
Color
TotalDownload

...

[multilib]
Include = /etc/pacman.d/mirrorlist
-------------------------

- Add under Misc options
-------------------------
ILoveCandy
-------------------------

pacman -Syyu

%%%%%%%%%%%%%%%%%%%%
%%%%% NetworkManager
%%%%%%%%%%%%%%%%%%%%

pacman -S 'networkmanager'

systemctl enable 'NetworkManager'

%%%%%%%%%%%%%%%%%%%%
%%%%% Intel Microcode
%%%%%%%%%%%%%%%%%%%%

pacman -S 'intel-ucode' --noconfirm

%%%%%%%%%%%%%%%%%%%%
%%%%% Root Password
%%%%%%%%%%%%%%%%%%%%

passwd

%%%%%%%%%%%%%%%%%%%%
%%%%% Create User
%%%%%%%%%%%%%%%%%%%%

useradd -m -G 'wheel' -s '/bin/bash' 'espionage724'
passwd 'espionage724'
chfn 'espionage724'

%%%%%%%%%%%%%%%%%%%%
%%%%% SSH
%%%%%%%%%%%%%%%%%%%%

pacman -S 'openssh' --noconfirm

nano '/etc/ssh/sshd_config'

-------------------------
PermitRootLogin no
-------------------------

systemctl enable 'sshd.socket'

%%%%%%%%%%%%%%%%%%%%
%%%%% Root Usage
%%%%%%%%%%%%%%%%%%%%

EDITOR='nano' visudo

- Uncomment
-------------------------
%wheel ALL=(ALL) ALL
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Nano as Default Editor
%%%%%%%%%%%%%%%%%%%%

nano '/etc/sudoers.d/nano'

-------------------------
Defaults editor=/usr/bin/nano
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Let Nano Wrap Text
%%%%%%%%%%%%%%%%%%%%

nano '/etc/nanorc'

- Uncomment
-------------------------
set nowrap
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Finalize Install (GPT/EFI Systems)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initramfs
%%%%%%%%%%%%%%%%%%%%

nano '/etc/mkinitcpio.conf'

- Add in-between 'block' and 'filesystems' hooks
- Remove 'encrypt' if not using disk encryption
-------------------------
encrypt lvm2
-------------------------

- Add
-------------------------
COMPRESSION="cat"
-------------------------

mkinitcpio -p 'linux'

%%%%%%%%%%%%%%%%%%%%
%%%%% Boot Loader Notes
%%%%%%%%%%%%%%%%%%%%

- Remove 'intel-ucode' if not using Intel
- Remove 'cryptdevice=' if not using disk encryption
- Remove 'usbhid.quirks=0x1B1C:0x1B22:0x20000000' for computers not using a Corsair mouse

%%%%%%%%%%%%%%%%%%%%
%%%%% Install Bootloader
%%%%%%%%%%%%%%%%%%%%

bootctl install

%%%%%%%%%%%%%%%%%%%%
%%%%% Linux Kernel
%%%%%%%%%%%%%%%%%%%%

nano '/boot/loader/entries/linux-kernel.conf'

-------------------------
title   Arch Linux (linux-kernel)
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
initrd  /intel-ucode.img
options cryptdevice=/dev/sda2:lvm root=/dev/mapper/arch-root rw quiet usbhid.quirks=0x1B1C:0x1B22:0x20000000
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Loader Defaults
%%%%%%%%%%%%%%%%%%%%

nano '/boot/loader/loader.conf'

-------------------------
timeout 3
default linux-kernel
editor  0
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Finalize Install (MBR/Legacy Unencrypted Systems)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initramfs
%%%%%%%%%%%%%%%%%%%%

nano '/etc/mkinitcpio.conf'

- Add in-between 'block' and 'filesystems' hooks
-------------------------
lvm2
-------------------------

- Add
-------------------------
COMPRESSION="cat"
-------------------------

mkinitcpio -p 'linux'

%%%%%%%%%%%%%%%%%%%%
%%%%% Boot Loader
%%%%%%%%%%%%%%%%%%%%

pacman -S 'grub' --noconfirm

grub-install --target='i386-pc' '/dev/sda'

nano '/etc/default/grub'

-------------------------
GRUB_TIMEOUT 3
-------------------------

grub-mkconfig -o '/boot/grub/grub.cfg'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Finish Up
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sync
exit
umount -R '/mnt'
reboot

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Post-Install
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Internet (Wirelss)
%%%%%%%%%%%%%%%%%%%%

sudo nmtui

%%%%%%%%%%%%%%%%%%%%
%%%%% SSH
%%%%%%%%%%%%%%%%%%%%

ip addr

ssh 192.168.1.1**

%%%%%%%%%%%%%%%%%%%%
%%%%% sp5100_tco Fix
%%%%%%%%%%%%%%%%%%%%

- Check dmesg; if flood of "No handler or method for GPE xx", do this
- Likely only affects AMD APU (Spinecrack, Hailrake)

https://bugzilla.kernel.org/show_bug.cgi?id=114201

sudo -e '/etc/modprobe.d/blacklist.conf'

-------------------------
blacklist sp5100_tco
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

sudo pacman -Syyu

%%%%%%%%%%%%%%%%%%%%
%%%%% Core (GUI)
%%%%%%%%%%%%%%%%%%%%

sudo pacman -S --needed xorg-server xf86-input-libinput mesa-libgl lib32-mesa-libgl

%%%%%%%%%%%%%%%%%%%%
%%%%% GNOME
%%%%%%%%%%%%%%%%%%%%

sudo pacman -S --needed gnome gnome-extra

gnome: 1 5-6 8-10 12-13 15-22 24-26 28-31 36 40-42 46

gnome-extra: 2 10 13-14 18-19 21 23 25 41-43 52
add 6 for cd-burner (brasero), 7 for webcam (cheese) on gnome-extra

%%%%%%%%%%%%%%%%%%%%
%%%%% Enable Services (GUI)
%%%%%%%%%%%%%%%%%%%%

sudo systemctl enable 'gdm' 'upower'

%%%%%%%%%%%%%%%%%%%%
%%%%% Network (Static)
%%%%%%%%%%%%%%%%%%%%

sudo nmtui

%%%%%%%%%%%%%%%%%%%%
%%%%% Hostname
%%%%%%%%%%%%%%%%%%%%

sudo hostnamectl set-hostname 'x'

%%%%%%%%%%%%%%%%%%%%
%%%%% Reboot
%%%%%%%%%%%%%%%%%%%%

sudo reboot

%%%%%%%%%%%%%%%%%%%%
%%%%% General (GUI)
%%%%%%%%%%%%%%%%%%%%

sudo pacman -S --needed cups transmission-gtk lollypop perl-image-exiftool filezilla libreoffice-fresh aria2 openssh hplip htop hdparm wget p7zip mpv ffmpeg ffmpegthumbnailer libx264 gst-libav wine-staging winetricks lib32-libpulse lib32-gnutls ttf-freefont ttf-liberation ttf-arphic-uming ttf-baekmuk ttf-indic-otf ttf-khmer ttf-symbola ntp hunspell-en mesa-demos ufw samba lib32-mpg123 lib32-gst-plugins-base-libs

%%%%%%%%%%%%%%%%%%%%
%%%%% Personal Machines
%%%%%%%%%%%%%%%%%%%%

android-tools android-udev gimp shotwell firefox keepass

%%%%%%%%%%%%%%%%%%%%
%%%%% Limited
%%%%%%%%%%%%%%%%%%%%

jre8-openjdk

%%%%%%%%%%%%%%%%%%%%
%%%%% Hammerstorm
%%%%%%%%%%%%%%%%%%%%

xf86-video-amdgpu handbrake

%%%%%%%%%%%%%%%%%%%%
%%%%% Hailrake
%%%%%%%%%%%%%%%%%%%%

alsa-tools

%%%%%%%%%%%%%%%%%%%%
%%%%% Piety
%%%%%%%%%%%%%%%%%%%%

libva-intel-driver chromium

%%%%%%%%%%%%%%%%%%%%
%%%%% ckb-git
%%%%%%%%%%%%%%%%%%%%

wget 'https://aur.archlinux.org/cgit/aur.git/snapshot/ckb-git.tar.gz' -O ~/'Downloads/ckb-git.tar.gz' && tar -xzvf ~/'Downloads/ckb-git.tar.gz' -C ~/'Downloads' && cd ~/'Downloads/ckb-git' && makepkg -sri --noconfirm && cd ~ && rm -Rf ~/'Downloads/ckb-git.tar.gz' ~/'Downloads/ckb-git' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Chromium Widevine
%%%%%%%%%%%%%%%%%%%%

wget 'https://aur.archlinux.org/cgit/aur.git/snapshot/chromium-widevine.tar.gz' -O ~/'Downloads/chromium-widevine.tar.gz' && tar -xzvf ~/'Downloads/chromium-widevine.tar.gz' -C ~/'Downloads' && cd ~/'Downloads/chromium-widevine' && makepkg -sri --noconfirm && cd ~ && rm -Rf ~/'Downloads/chromium-widevine.tar.gz' ~/'Downloads/chromium-widevine' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Chromium Pepper Flash
%%%%%%%%%%%%%%%%%%%%

wget 'https://aur.archlinux.org/cgit/aur.git/snapshot/chromium-pepper-flash.tar.gz' -O ~/'Downloads/chromium-pepper-flash.tar.gz' && tar -xzvf ~/'Downloads/chromium-pepper-flash.tar.gz' -C ~/'Downloads' && cd ~/'Downloads/chromium-pepper-flash' && makepkg -sri --noconfirm && cd ~ && rm -Rf ~/'Downloads/chromium-pepper-flash.tar.gz' ~/'Downloads/chromium-pepper-flash' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Oak, Kraityn, Hatebeat
%%%%%%%%%%%%%%%%%%%%

sudo pacman -S --needed htop hdparm wget p7zip aria2 ufw unzip

%%%%%%%%%%%%%%%%%%%%
%%%%% Hatebeat
%%%%%%%%%%%%%%%%%%%%

alsa-utils xorg-server xorg-xinit mesa-libgl xf86-input-libinput

%%%%%%%%%%%%%%%%%%%%
%%%%% Master Password
%%%%%%%%%%%%%%%%%%%%

wget 'https://ssl.masterpasswordapp.com/masterpassword-gui.jar' -O ~/'Documents/masterpassword-gui.jar' && chmod +x ~/'Documents/masterpassword-gui.jar'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Settings Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Keyboard
%%%%%%%%%%%%%%%%%%%%

gnome-terminal
Ctrl + Alt + T

gnome-shell --replace
Ctrl + Alt + \

java -jar '/home/espionage724/Documents/masterpassword-gui.jar'
Ctrl + Alt + Z

%%%%%%%%%%%%%%%%%%%%
%%%%% Network
%%%%%%%%%%%%%%%%%%%%

Indoor DNS: 192.168.1.158
Outdoor DNS: https://www.opennicproject.org

%%%%%%%%%%%%%%%%%%%%
%%%%% Printer
%%%%%%%%%%%%%%%%%%%%

sudo systemctl enable 'org.cups.cupsd' && sudo systemctl start 'org.cups.cupsd'

http://localhost:631/admin

HP Envy 4500 Series, hpcups (en)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Other Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Files
%%%%%%%%%%%%%%%%%%%%

Sort folders before files and by type
1GB file previews

%%%%%%%%%%%%%%%%%%%%
%%%%% gedit
%%%%%%%%%%%%%%%%%%%%

4 Tab length and use spaces instead of tabs

%%%%%%%%%%%%%%%%%%%%
%%%%% Shotwell (MediaGoblin)
%%%%%%%%%%%%%%%%%%%%

Piwigo
https://media.realmofespionage.xyz/api/piwigo/ws.php

%%%%%%%%%%%%%%%%%%%%
%%%%% Transmission
%%%%%%%%%%%%%%%%%%%%

http://john.bitsurge.net/public/biglist.p2p.gz

%%%%%%%%%%%%%%%%%%%%
%%%%% mpv
%%%%%%%%%%%%%%%%%%%%

mkdir -p ~/'.config/mpv' && nano ~/'.config/mpv/mpv.conf'

- Change 'vdpau' to 'vaapi' for VA-API
-------------------------
hwdec='vdpau'
vo='opengl-hq'
ao='pulse'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% UFW (GUI/Local)
%%%%%%%%%%%%%%%%%%%%

sudo ufw reset && sudo ufw default deny && sudo ufw enable && sudo ufw logging off && sudo systemctl enable 'ufw'

%%%%%%%%%%%%%%%%%%%%
%%%%% UFW (CLI/SSH)
%%%%%%%%%%%%%%%%%%%%

sudo ufw reset && sudo ufw default deny && sudo ufw allow from '192.168.1.0/24' to any port '22' && sudo ufw enable && sudo ufw logging off && sudo systemctl enable 'ufw'

%%%%%%%%%%%%%%%%%%%%
%%%%% Epiphany
%%%%%%%%%%%%%%%%%%%%

gsettings set org.gnome.Epiphany keyword-search-url "https://searx.me/?q=%s"

gsettings set org.gnome.Epiphany keyword-search-url "https://startpage.com/do/search?q=%s"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Secure Shell
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Generate SSH Keys
%%%%%%%%%%%%%%%%%%%%

ssh-keygen -o -t 'ed25519'

%%%%%%%%%%%%%%%%%%%%
%%%%% Backup SSH Keys
%%%%%%%%%%%%%%%%%%%%

cd ~ && tar -cvzf ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' '.ssh' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Restore SSH Keys
%%%%%%%%%%%%%%%%%%%%

cd ~ && mv ~/'.ssh' ~/'ssh-bak' && tar -xvzf ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' '.ssh' && rm ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Copy SSH PubKey
%%%%%%%%%%%%%%%%%%%%

ssh-copy-id 'espionage724@192.168.1.152'
ssh-copy-id 'espionage724@192.168.1.153'
ssh-copy-id 'pi@192.168.1.158'
ssh-copy-id 'espionage724@192.168.1.163'
ssh-copy-id 'beowulfsdr@192.168.1.166'

%%%%%%%%%%%%%%%%%%%%
%%%%% Force PubKey Auth
%%%%%%%%%%%%%%%%%%%%

- Only do after copying ssh keys from main computers

ssh 'espionage724@192.168.1.152'
ssh 'espionage724@192.168.1.153'
ssh 'pi@192.168.1.158'
ssh 'espionage724@192.168.1.163'
ssh 'beowulfsdr@192.168.1.166'

sudo -e '/etc/ssh/sshd_config'

-------------------------
PasswordAuthentication no
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Machine Key Labels
%%%%%%%%%%%%%%%%%%%%

sh 'espionage724@192.168.1.152'
ssh 'espionage724@192.168.1.153'
ssh 'pi@192.168.1.158'
ssh 'espionage724@192.168.1.163'
ssh 'beowulfsdr@192.168.1.166'

nano ~/'.ssh/authorized_keys'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Environment Variables
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% ATI/AMD GPU Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/profile.d/custom.sh'

-------------------------
# Radeon Tweaks
export R600_DEBUG='sbcl,hyperz,llvm,sisched,forcedma'
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Swappiness
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/sysctl.d/swappiness.conf'

-------------------------
vm.swappiness = 10
vm.vfs_cache_pressure = 50
-------------------------

cat '/proc/sys/vm/swappiness'
cat '/proc/sys/vm/vfs_cache_pressure'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Kernel Hardening
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/sysctl.d/harden.conf'

-------------------------
kernel.dmesg_restrict = 1
kernel.kptr_restrict = 1
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Disable Wayland on GDM
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/gdm/custom.conf

-------------------------
[daemon]
WaylandEnable=false
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hammerstorm (desktop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo systemctl mask 'colord' && sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier      "Caribbean Islands"
    Driver          "amdgpu"
    Option          "DRI"           "3"
    Option          "TearFree"      "true"
EndSection

Section "Monitor"
    Identifier      "HDMI-A-0"
    Gamma           0.8
EndSection

Section "Monitor"
    Identifier      "DVI-D-0"
    Gamma           0.8
EndSection

Section "Monitor"
    Identifier      "DVI-I-1"
    Gamma           0.8
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% GDM Monitor Conf Copy
%%%%%%%%%%%%%%%%%%%%

sudo mkdir -p ~gdm/'.config' && sudo cp ~/'.config/monitors.xml' ~gdm/'.config/monitors.xml'

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/bin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Spinecrack (laptop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo systemctl mask 'colord' && sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier      "Northern Islands"
    Driver          "modesetting"
EndSection

Section "Monitor"
    Identifier      "eDP-1"
    Gamma           0.8
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/bin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/bin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Corsair Mouse Fix
%%%%%%%%%%%%%%%%%%%%

sudo -e '/boot/loader/entries/linux-kernel.conf'

- Add to options
-------------------------
usbhid.quirks=0x1B1C:0x1B22:0x20000000
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hailrake (2-in-1)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier      "Sea Islands"
    Driver          "modesetting"
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Audio Fix
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hp-audio.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hp-audio' && sudo systemctl start 'hp-audio' && sudo systemctl status 'hp-audio' -l

-------------------------
[Unit]
Description=HP Audio Bass and Volume Fix

[Service]
Type=oneshot
ExecStart='/usr/bin/hda-verb' '/dev/snd/hwC1D0' '0x1a' '0x782' '0x61'
ExecStart='/usr/bin/hda-verb' '/dev/snd/hwC1D0' '0x1a' '0x773' '0x2d'

[Install]
WantedBy=multi-user.target
EOF
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/bin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Piety (mom's laptop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo systemctl mask 'colord' && sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier      "Haswell"
    Driver          "modesetting"
EndSection

Section "Monitor"
    Identifier      "eDP-1"
    Gamma           0.8
EndSection
----------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/bin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/bin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hatebeat (StepMania)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier      "R600"
    Driver          "modesetting"
EndSection

Section "Monitor"
    Identifier      "DVI-I-1
    Modeline        "640x480_120.00"  52.41  640 680 744 848  480 481 484 515  -HSync +Vsync
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% ASUS Audio
%%%%%%%%%%%%%%%%%%%%

sudo amixer cset 'numid=14' '2'
sudo amixer set 'Headphone' '100%'

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/bin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/bin/hdparm' -M '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Oak (server)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/bin/hdparm' -B '255' '/dev/sda'
ExecStart='/usr/bin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Kraityn (server)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sda'
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sdb'
ExecStart='/usr/bin/hdparm' -A '1' '/dev/sdc'
ExecStart='/usr/bin/hdparm' -B '255' '/dev/sdb'
ExecStart='/usr/bin/hdparm' -B '255' '/dev/sdc'
ExecStart='/usr/bin/hdparm' -M '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -R '0' '/dev/sdb'
ExecStart='/usr/bin/hdparm' -R '0' '/dev/sdc'
ExecStart='/usr/bin/hdparm' -s '0' '/dev/sdb'
ExecStart='/usr/bin/hdparm' -s '0' '/dev/sdc'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sda'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sdb'
ExecStart='/usr/bin/hdparm' -S '0' '/dev/sdc'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sda'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sdb'
ExecStart='/usr/bin/hdparm' -W '1' '/dev/sdc'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% PulseAudio
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Config
%%%%%%%%%%%%%%%%%%%%

nano ~/'.config/pulse/daemon.conf'

-------------------------
resample-method = soxr-mq
flat-volumes = no
deferred-volume-safety-margin-usec = 1
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Volume (Output)
%%%%%%%%%%%%%%%%%%%%

pactl list 'short' 'sinks'

pactl set-sink-mute '1' '0'
pactl set-sink-volume '1' 60%

%%%%%%%%%%%%%%%%%%%%
%%%%% Volume (Input)
%%%%%%%%%%%%%%%%%%%%

pactl list 'short' 'sources'

pactl set-source-mute '2' '0'
pactl set-source-volume '2' 35%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Automatic Updates
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Times
%%%%%%%%%%%%%%%%%%%%

- Hammerstorm:  05:00:00
- Spinecrack:   05:10:00
- Hailrake:     05:20:00
- Piety:        05:30:00
- Hatebeat:     05:40:00
- Thistlesage:  05:50:00
- Oak:          06:00:00
- Kraityn:      06:10:00

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/arch-up.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/pacman' -Sc --noconfirm
ExecStart='/usr/bin/pacman' -Syyu --noconfirm
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/arch-up.timer'

-------------------------
[Unit]
Description=Daily pacman sync and update

[Timer]
OnCalendar=*-*-* 05:x0:00
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Enable/Start
%%%%%%%%%%%%%%%%%%%%

sudo systemctl daemon-reload && sudo systemctl enable 'arch-up.timer' && sudo systemctl start 'arch-up.timer'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Steven Black's Unified Hosts File
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hosts-up.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/wget' 'https://github.com/StevenBlack/hosts/archive/master.zip' -O '/tmp/master.zip'
ExecStart='/usr/bin/unzip' '/tmp/master.zip' -d '/tmp'
ExecStart='/usr/bin/python2' '/tmp/hosts-master/updateHostsFile.py' --auto --replace
ExecStart='/usr/bin/rm' -R '/tmp/master.zip' '/tmp/hosts-master'
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/hosts-up.timer'

-------------------------
[Unit]
Description=Weekly hosts source refresh

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Enable/Start
%%%%%%%%%%%%%%%%%%%%

sudo systemctl daemon-reload && sudo systemctl enable 'hosts-up.timer' && sudo systemctl start 'hosts-up' 'hosts-up.timer' && sudo systemctl status 'hosts-up' -l

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% XFS Defragmentation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service (General)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/xfs-m.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/xfs_fsr' '/dev/mapper/arch-root'
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Service (Kraityn)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/xfs-m.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/xfs_fsr' '/dev/mapper/arch-root'
ExecStart='/usr/bin/xfs_fsr' '/dev/sdb1'
ExecStart='/usr/bin/xfs_fsr' '/dev/sdc1'
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/lib/systemd/system/xfs-m.timer'

-------------------------
[Unit]
Description=Weekly XFS Defrag

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Enable/Start
%%%%%%%%%%%%%%%%%%%%

sudo systemctl daemon-reload && sudo systemctl enable 'xfs-m.timer' && sudo systemctl start 'xfs-m' 'xfs-m.timer' && sudo systemctl status 'xfs-m' -l

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Firefox
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% about:config
%%%%%%%%%%%%%%%%%%%%

-------------------------
bash -c 'cat >  ~/"user.js"' << EOF
user_pref("media.mp3.enabled", true);
user_pref("layout.frame_rate.precise", true);
user_pref("mousewheel.min_line_scroll_amount", 40);
user_pref("javascript.options.mem.high_water_mark", 32);
user_pref("javascript.options.mem.max", 51200);
user_pref("dom.storage.enabled", true);
user_pref("dom.event.clipboardevents.enabled", true);
user_pref("media.fragmented-mp4.exposed", true);
user_pref("media.fragmented-mp4.ffmpeg.enabled", true);
user_pref("media.mediasource.ignore_codecs", true);
user_pref("layers.acceleration.force-enabled", true);
user_pref("layers.offmainthreadcomposition.enabled", true);
EOF
mv ~/'user.js' ~/'.mozilla/firefox/'*'.default'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Extenstions (7)
%%%%%%%%%%%%%%%%%%%%

xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/privacy-settings/versions' && xdg-open 'https://www.eff.org/privacybadger' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/versions' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/gnotifier/versions' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/versions' && xdg-open 'https://www.eff.org/https-everywhere' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/disable-hello-pocket-reader/versions'

%%%%%%%%%%%%%%%%%%%%
%%%%% Extensions Configuration
%%%%%%%%%%%%%%%%%%%%

- uBlock filters (disable EasyList + EasyPrivacy, enable Easy+Fanboy Ultimate + both Adblock killers)
- Full privacy with Privacy Settings button

%%%%%%%%%%%%%%%%%%%%
%%%%% Search Engines
%%%%%%%%%%%%%%%%%%%%

https://searx.me/about#
https://startpage.com/eng/download-startpage-plugin.html?hmb=1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Noteable Folders and Commands
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Shortcuts
%%%%%%%%%%%%%%%%%%%%

/usr/share/applications
~/.local/share/applications

%%%%%%%%%%%%%%%%%%%%
%%%%% Icons
%%%%%%%%%%%%%%%%%%%%

/usr/share/icons/hicolor
~/.local/share/icons/hicolor

%%%%%%%%%%%%%%%%%%%%
%%%%% systemd Scripts
%%%%%%%%%%%%%%%%%%%%

/etc/systemd/system
/usr/lib/systemd/system

%%%%%%%%%%%%%%%%%%%%
%%%%% Check CPU Frequency
%%%%%%%%%%%%%%%%%%%%

grep 'MHz' '/proc/cpuinfo'
watch -n 0.1 grep \'cpu MHz\' '/proc/cpuinfo'

%%%%%%%%%%%%%%%%%%%%
%%%%% Encryption Info
%%%%%%%%%%%%%%%%%%%%

sudo cryptsetup luksDump '/dev/sda2'
sudo cryptsetup status '/dev/mapper/lvm'

%%%%%%%%%%%%%%%%%%%%
%%%%% Hard Drive Wipe
%%%%%%%%%%%%%%%%%%%%

- Make sure to change sda if not wanting to do sda

sudo hdparm -I '/dev/sda' | grep 'not'
sudo hdparm --user-master u --security-set-pass 'x' '/dev/sda'
sudo hdparm --user-master u --security-erase-enhanced 'x' '/dev/sda'

%%%%%%%%%%%%%%%%%%%%
%%%%% Xorg Log
%%%%%%%%%%%%%%%%%%%%

journalctl -e _COMM='gdm-x-session'

%%%%%%%%%%%%%%%%%%%%
%%%%% Get GCC Compile Flags
%%%%%%%%%%%%%%%%%%%%

gcc -v -E -x c -march=native -mtune=native - < /dev/null 2>&1 | grep cc1 | perl -pe 's/ -mno-\S+//g; s/^.* - //g;'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Privacy
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove metadata
%%%%%%%%%%%%%%%%%%%%

exiftool -all= *.* -overwrite_original

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################