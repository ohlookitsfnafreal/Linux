%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- MariaDB
- Realm of Espionage
- Fedora Server 23

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Other Notes
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Show Users (mysql command)
select User,Host from mysql.user;

- Socket File
/var/lib/mysql/mysql.sock

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo dnf install 'mariadb-server'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Firewall
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo firewall-cmd --permanent --add-service='mysql' && sudo firewall-cmd --reload

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Initial Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/my.cnf.d/custom.cnf'

-------------------------
[client]
default-character-set = utf8mb4

[mysql]
default-character-set = utf8mb4

[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
expire_logs_days = 7
max_binlog_size = 100M
-------------------------

- TODO: Check optimal setting
innodb_buffer_pool_size = 1G

%%%%%%%%%%%%%%%%%%%%
%%%%% Verify
%%%%%%%%%%%%%%%%%%%%

mysqladmin variables -u 'root' -p | egrep 'utf8|networking'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Service
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo systemctl enable 'mariadb'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% MariaDB Secure
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo systemctl start 'mariadb' && mysql_secure_installation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database Repair and Optimize
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Create User
%%%%%%%%%%%%%%%%%%%%

mysql -u 'root' -p

GRANT SELECT, INSERT ON *.* to 'maintenance'@'localhost' IDENTIFIED BY 'x';

FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%
%%%%% Authentication
%%%%%%%%%%%%%%%%%%%%

sudo mkdir -p '/var/lib/mysqlauth' && sudo chown 'mysql':'mysql' '/var/lib/mysqlauth' && sudo -e '/var/lib/mysqlauth/maintenance' && sudo chmod '600' '/var/lib/mysqlauth/maintenance'

-------------------------
[mysqlcheck]
user=maintenance
password=x
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/db-m.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/mysqlcheck' --defaults-extra-file='/var/lib/mysqlauth/maintenance' --auto-repair --optimize --all-databases --force
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/db-m.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'db-m.timer' && sudo systemctl start 'db-m' 'db-m.timer' && sudo systemctl status 'db-m' -l

-------------------------
[Unit]
Description=Weekly database repair and maintenance

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database Backup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% fstab
%%%%%%%%%%%%%%%%%%%%

sudo mkdir -p '/mnt/USB' && sudo -e '/etc/fstab'

-------------------------
# Flash Drive Backup
/dev/sdb1 /mnt/USB xfs rw,relatime,attr2,inode64,noquota 0 2
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Filesystem Defrag
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/fs-m.service' && sudo systemctl daemon-reload

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################