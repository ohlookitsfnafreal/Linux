%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- GNU social setup for Realm of Espionage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Git Management
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Download
%%%%%%%%%%%%%%%%%%%%

cd '/var/www' && sudo -u apache git clone -b nightly https://git.gnu.io/gnu/gnu-social.git '/var/www/html/social' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Update
%%%%%%%%%%%%%%%%%%%%

cd '/var/www/html/social' && sudo -u apache git pull origin nightly && cd '/home/espionage724' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

CREATE DATABASE gnusocial;

CREATE USER gnusocial;
SET PASSWORD FOR 'gnusocial' = PASSWORD ('x');

GRANT ALL PRIVILEGES ON gnusocial.* to 'gnusocial'@'192.168.1.153' IDENTIFIED BY 'x';

FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% URL Rewrite
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo nano '/etc/httpd/conf.d/gnusocial.conf'

-------------------------
<Directory "/var/www/html/social">
    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule (.*) index.php?p=$1 [L,QSA]
    
    <FilesMatch "\.(ini)">
        <IfVersion < 2.3>
            Order allow,deny
            Deny from all
        </IfVersion>
        <IfVersion >= 2.3>
            Require all denied
        </IfVersion>
    </FilesMatch>
</Directory>
-------------------------

sudo systemctl restart httpd

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Make folders
%%%%%%%%%%%%%%%%%%%%

sudo -u apache mkdir '/var/www/html/social/avatar' '/var/www/html/social/background' '/var/www/html/social/file'

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Setup
%%%%%%%%%%%%%%%%%%%%

- Go to https://www.realmofespionage.com/social

%%%%%%%%%%%%%%%%%%%%
%%%%% Post-Setup
%%%%%%%%%%%%%%%%%%%%

sudo -u apache nano '/var/www/html/social/config.php'

-------------------------
$config['db']['schemacheck'] = 'script';

$config['mail']['backend'] = 'smtp';
$config['mail']['params'] = array(
'host' => 'smtp.sendgrid.net',
'port' => 587,
'auth' => true,
'username' => 'Espionage724',
'password' => 'x'
);

addPlugin('EmailRegistration');
addPlugin('GeoURL');
addPlugin('ModPlus');
addPlugin('ChooseTheme');
-------------------------

sudo -u apache php '/var/www/html/social/scripts/checkschema.php'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% License Footer
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Logo
%%%%%%%%%%%%%%%%%%%%

sudo -u apache mkdir '/var/www/html/images/servers'
sudo -u apache wget https://licensebuttons.net/l/by-sa/4.0/80x15.png -O '/var/www/html/images/servers/cc-by-sa-40.png'

%%%%%%%%%%%%%%%%%%%%
%%%%% Footer Text
%%%%%%%%%%%%%%%%%%%%

License Text: Creative Commons Attribution-ShareAlike 4.0 International
License URL: https://creativecommons.org/licenses/by-sa/4.0
License Image URL: https://www.realmofespionage.com/images/servers/cc-by-sa-40.png

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################