%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- WoW account registration and status setup for Realm of Espionage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Git Management
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TODO: Fork own project and make private git to do changes in :p

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Download
%%%%%%%%%%%%%%%%%%%%

sudo mkdir '/var/www/wow' && sudo chown nginx:nginx '/var/www/wow'
cd '/var/www' && sudo -u nginx git clone -b TODO '/var/www/wow' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Update
%%%%%%%%%%%%%%%%%%%%

cd '/var/www/wow' && sudo -u nginx git pull origin TODO && cd '/home/espionage724' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

CREATE USER soap;
SET PASSWORD FOR 'soap' = PASSWORD ('x');

GRANT SELECT, UPDATE on auth.account to 'soap'@'%' IDENTIFIED BY 'X';

FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% nginx
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo nano '/etc/nginx/conf.d/wow.conf'

-------------------------
server {
    listen 443 ssl http2;
    server_name wow.realmofespionage.com;
    server_name_in_redirect off;
    root /var/www/wow;
    index index.php;

    #access_log /var/log/nginx/access.log main;
    #error_log /var/log/nginx/error.log info;

    add_header X-Frame-Options "ALLOW-FROM https://realmofespionage.com";

    location / {
        try_files $uri.php $uri/ =404;
    }

    location /config.php {
        deny all;
    }

    location /SOAPRegistration.php {
        deny all;
    }

    location /status {
        try_files $uri.php $uri/ =404;
    }

    location /site.css {
        add_header  Content-Type    text/css;
    }

    location ~ \.php$ {
        fastcgi_pass unix:/run/php-fpm/www.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi.conf;
    }
}
-------------------------

sudo systemctl restart nginx php-fpm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -u nginx nano '/var/www/wow/config.php'

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################